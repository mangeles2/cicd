//
//  SampleTest.swift
//  CICDTests
//
//  Created by Yiel Miranda on 6/8/21.
//

import Foundation
import Nimble
import Quick

@testable import CICD

class SampleTest: QuickSpec {
    override func spec() {
        describe("Artihmetic Progression") {
            context("addition process should be performed") {
                let addArithmetic = Arithmetic(numOne: 10, numTwo: 3)
                it("Add the two int using the class function and use equal to validate result") {
                    expect(addArithmetic.addition()).to(equal(13))
                }
            }
        }
    }
}
