//
//  Artihmetic.swift
//  CICDTests
//
//  Created by Yiel Miranda on 6/8/21.
//

import Foundation

class Arithmetic {
    let numberOne: Int!
    let numberTwo: Int!
    
    init(numOne: Int, numTwo: Int) {
        self.numberOne = numOne
        self.numberTwo = numTwo
    }
    
    func addition() -> Int {
        return numberOne + numberTwo
    }
}
